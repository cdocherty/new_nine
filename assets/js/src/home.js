$(document).ready(function() {
    $(".fancy-open").fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        width: 900,
        height: 900,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 720) {
            $("header").addClass("active");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $("header").removeClass("active");
        }
    });

    $('a').click(function() {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });
});